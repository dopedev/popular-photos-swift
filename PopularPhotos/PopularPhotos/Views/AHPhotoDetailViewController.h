//
//  AHPhotoDetailViewController.h
//  
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import <UIKit/UIKit.h>
#import "AHPhotoViewModelProtocol.h"

@interface AHPhotoDetailViewController : UIViewController

@property (nonatomic, strong) id<AHPhotoViewModelProtocol> viewModel;

@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong, readonly) UILabel *descriptionlabel;

@end
