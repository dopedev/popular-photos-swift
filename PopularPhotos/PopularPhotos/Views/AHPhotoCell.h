//
//  AHPhotoCell.h
//  
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import <UIKit/UIKit.h>
#import "AHPhotoViewModelProtocol.h"

/**
 *  This is a table view cell that displays a photo
 */
@interface AHPhotoCell : UITableViewCell

/**
 *  The view model that backs this view
 */
@property (nonatomic, strong) id<AHPhotoViewModelProtocol> viewModel;

/**
 *  The image view that displays the photo
 */
@property (nonatomic, strong, readonly) UIImageView *photoImageView;

@end
