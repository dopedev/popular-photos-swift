//
//  AHPhotoCell.m
//
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import "AHPhotoCell.h"
#import <ReactiveCocoa.h>
#import <RACEXTScope.h>
#import <Masonry.h>

static CGFloat const kImageViewPadding = 5;

@interface AHPhotoCell ()

@property (nonatomic, strong, readwrite) UIImageView *photoImageView;

- (void)setupView;
- (void)setBindings;

@end

@implementation AHPhotoCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupView];
        [self setBindings];
    }
    
    return self;
}

- (void)setupView {
    self.photoImageView = [UIImageView new];
    self.photoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.photoImageView];
    
    @weakify(self)
    [self.photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(0, 0, kImageViewPadding, 0));
    }];
}

- (void)setBindings {
    @weakify(self)
    [[RACObserve(self, viewModel.downloadedPhoto) ignore:nil]
     subscribeNext:^(id x) {
         
         // animate all image chagnes
         [UIView animateWithDuration:0.2
                               delay:0.05
                             options:UIViewAnimationOptionCurveEaseInOut
                          animations:^{
                              @strongify(self)
                              self.photoImageView.alpha = 0.0f;
                              self.photoImageView.image = x;
                              self.photoImageView.alpha = 1.0f;
                          } completion:nil];
         
     }];
    
    [[RACObserve(self, viewModel.cachedPhoto) ignore:nil]
     subscribeNext:^(id x) {
        @strongify(self)
         self.photoImageView.image = x;
    }];
}

- (void)prepareForReuse {
    self.photoImageView.image = nil;
}

@end
