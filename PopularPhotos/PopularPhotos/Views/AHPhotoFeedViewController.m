//
//  AHPhotoFeedViewController.m
//
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import "AHPhotoFeedViewController.h"
#import <RACEXTScope.h>
#import "AHPhotoCell.h"
#import "AHPhotoDetailViewController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <Masonry.h>
#import "AHZoomTransition.h"

@interface AHPhotoFeedViewController ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UINavigationControllerDelegate>

/**
 *  Custom "zooming" transition.
 */
@property (nonatomic, strong) AHZoomTransition *transition;

/**
 *  Sets the bindings for the view.
 */
- (void)setBindings;

@end

@implementation AHPhotoFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.navigationController.delegate = self;
    self.tableView.alwaysBounceVertical = YES;
    self.refreshControl = [UIRefreshControl new];
    
    self.transition = [AHZoomTransition new];
    
    [self setBindings];
    
    [[self.viewModel refreshFeedCommand] execute:nil];
}

- (void)setBindings {
    @weakify(self)
    
    [self.viewModel.reloadViewsSignal
     subscribeNext:^(id x) {
         @strongify(self)
         [self.tableView reloadData];
     }];
    
    [self.viewModel.insertSectionsSignal
     subscribeNext:^(NSIndexSet *indexSet) {
        @strongify(self)
         [self.tableView beginUpdates];
         [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationRight];
         [self.tableView endUpdates];
    }];
    
    self.refreshControl.rac_command = [self.viewModel refreshFeedCommand];
    
    // show footer when next page is executing
    
    [[[self.viewModel nextPageCommand] executing]
     subscribeNext:^(NSNumber *executing) {
         @strongify(self)
         
         if (executing.boolValue) {
             UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 50)];
             footer.backgroundColor = [UIColor clearColor];
             
             UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
             [activityIndicator startAnimating];
             [footer addSubview:activityIndicator];
             activityIndicator.center = footer.center;
             
             self.tableView.tableFooterView = footer;
         } else {
             self.tableView.tableFooterView = nil;
         }
     }];
    
    [RACObserve(self, viewModel.emptyStateText)
     subscribeNext:^(id x) {
        @strongify(self)
         [self.tableView reloadEmptyDataSet];
    }];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfItemsInSection:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.viewModel numberOfSections];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AHPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[AHPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.viewModel = [self.viewModel viewModelForPhotoAtIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AHPhotoDetailViewController *viewController = [AHPhotoDetailViewController new];
    viewController.viewModel = [self.viewModel viewModelForPhotoAtIndexPath:[self.tableView indexPathForSelectedRow]];
    
    [self.transition setExpandingCell:(AHPhotoCell *)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]] indexPath:[self.tableView indexPathForSelectedRow] fromTableView:self.tableView inView:self.view];
    
    viewController.transitioningDelegate = self.transition;
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150.0f;
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat horizontalOffset = self.tableView.contentOffset.y;
    
    if (self.tableView.contentSize.height - self.tableView.frame.size.height < horizontalOffset && horizontalOffset > 0) {
        [[self.viewModel nextPageCommand] execute:nil];
    }
}

#pragma mark - DZNEmptyDataSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:self.viewModel.emptyStateText];
}


- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView {
    [[self.viewModel refreshFeedCommand] execute:nil];
}

#pragma mark - UINavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
 
    if (operation == UINavigationControllerOperationPush) {
        return self.transition;
    }
    
    return nil;
}

@end