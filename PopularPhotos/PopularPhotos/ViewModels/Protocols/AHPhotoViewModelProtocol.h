//
//  AHPhotoViewModelProtocol.h
//  
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <ReactiveCocoa.h>

/**
 *  This defines an interface for a ViewModel used to back a View that displays a photo.
 */
@protocol AHPhotoViewModelProtocol <NSObject>

/**
 *  The photo to display if the image was cached. The view should bind to this prooperty and display the cached photo.
 */
@property (nonatomic, strong, readonly) UIImage *cachedPhoto;

/**
 *  The photo the view should display. The view should bind to this property to update the view as the image is downlaoded.
 */
@property (nonatomic, strong, readonly) UIImage *downloadedPhoto;

/**
 *  The view should bind to this property to display the name of the photo.
 */
@property (nonatomic, copy, readonly) NSString *name;

/**
 *  The view should bind to this property to display the date the photo was created.
 */
@property (nonatomic, copy, readonly) NSString *dateCreatedString;

/**
 *  The view should bind to this property to display the caption for the photo.
 */
@property (nonatomic, copy, readonly) NSString *caption;

/**
 *  The view should bind to this property to display the number of likes the photo received.
 */
@property (nonatomic, copy, readonly) NSString *numberOfLikesString;

/**
 *  The view should bind to this property to display the number of comments left on the photo.
 */
@property (nonatomic, copy, readonly) NSString *numberOfCommentsString;

@end
