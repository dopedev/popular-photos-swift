//
//  AHPhotoFeedViewModel.h
//  
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import <Foundation/Foundation.h>
#import "AHPhotoFeedViewModelProtocol.h"

/**
 *  This is a view model implementation of a feed view that displays photos from the 500px service
 */
@interface AHPhotoFeedViewModel : NSObject <AHPhotoFeedViewModelProtocol>

/**
 *  The view should bind to this property and display this when the feed is empty
 */
@property (nonatomic, copy, readonly) NSString *emptyStateText;

/**
 *  This is a hot signal that will emit a signal whenever the view model wants the view to refresh its views. The 'next' sends nil.
 */
@property (nonatomic, strong, readonly) RACSignal *reloadViewsSignal;

/**
 *  This is a hot signal that will emit a signal whenever new sections should be inserted into the list view. The view should bind to this signal and do the inserts whenever it recieves a signal. The signal will send the NSIndexSet to insert.
 */
@property (nonatomic, strong, readonly) RACSignal *insertSectionsSignal;

/**
 *  Tells the view model that the view wants to refresh the page. The view should connect the refresh control's raccommand to this command.
 *
 *  @return A command whose execute signal will complete when the network request finishes.
 */
- (RACCommand *)refreshFeedCommand;

/**
 *  Tells the view model that the view wants to request the next page of photo posts.
 *
 *  @return A command that will disable when the last page has been loaded.
 */
- (RACCommand *)nextPageCommand;

/**
 *  Returns the view model that backs a photo view.
 *
 *  @param indexPath The index path of the photo.
 *
 *  @return The view model.
 */
- (id<AHPhotoViewModelProtocol>)viewModelForPhotoAtIndexPath:(NSIndexPath *)indexPath;

- (NSUInteger)numberOfItemsInSection:(NSUInteger)section;
- (NSUInteger)numberOfSections;

@end
