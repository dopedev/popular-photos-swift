//
//  AHPhotoFeedViewModel.m
//
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import "AHPhotoFeedViewModel.h"
#import "AHPhotosClient.h"
#import "AHPhotoViewModel.h"

#import <RACEXTScope.h>

@interface AHPhotoFeedViewModel ()

#pragma mark - Writable property redeclarations
@property (nonatomic, copy, readwrite) NSString *emptyStateText;
@property (nonatomic, strong, readwrite) RACSubject *reloadViewsSignal;
@property (nonatomic, strong, readwrite) RACSubject *insertSectionsSignal;
@property (nonatomic, strong) RACCommand *refreshFeedCommand;
@property (nonatomic, strong) RACCommand *nextPageCommand;

@property (nonatomic, strong) AHPhotosClient *client;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) AHPhotoPage *currentPage;

/**
 *  Sets the necessary bindings.
 */
- (void)setBindings;

@end

@implementation AHPhotoFeedViewModel

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.client = [AHPhotosClient new];
        [self setBindings];
    }
    
    return self;
}

- (void)setBindings {
    @weakify(self)
    
    self.insertSectionsSignal = [RACSubject subject];
    self.reloadViewsSignal = [RACSubject subject];
    self.emptyStateText = @"";
    
    self.refreshFeedCommand = [[RACCommand alloc] initWithEnabled:nil
                                                      signalBlock:^RACSignal *(id input) {
                                                          return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                                                              
                                                              [[self.client getPageOfPopularPhotos:25 pageNumber:0] subscribeNext:^(AHPhotoPage *page) {
                                                                  
                                                                  self.currentPage = page;
                                                                  self.photos = page.photos;
                                                                  
                                                                  [(RACSubject *)self.reloadViewsSignal sendNext:nil];
                                                                                                                                    
                                                              } error:^(NSError *error) {
                                                                  [subscriber sendError:error];
                                                              } completed:^{
                                                                  [subscriber sendCompleted];
                                                              }];
                                                              
                                                              return [RACDisposable new];
                                                          }];
                                                      }];
    
    self.nextPageCommand = [[RACCommand alloc] initWithEnabled:nil
                                                   signalBlock:^RACSignal *(id input) {
                                                       return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                                                           @strongify(self)
                                                           
                                                           [[self.client getPageOfPopularPhotos:25 pageNumber:self.currentPage.currentPageNumber.integerValue + 1] subscribeNext:^(AHPhotoPage *page) {
                                                               self.currentPage = page;
                                                               
                                                               NSIndexSet *newIndexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(self.photos.count, self.currentPage.photos.count)];
                                                               
                                                               NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:self.photos];
                                                               self.photos = [mutableArray arrayByAddingObjectsFromArray:page.photos];
                                                               
                                                               [(RACSubject *)self.insertSectionsSignal sendNext:newIndexes];
                                                               
                                                           } error:^(NSError *error) {
                                                               [subscriber sendError:error];
                                                           } completed:^{
                                                               [subscriber sendCompleted];
                                                           }];
                                                           
                                                           return [RACDisposable new];
                                                       }];
                                                   }];
    
    // handle empty states
    
    // list is empty because it's initially loading
    RACSignal *loadingSignal = [[[self.refreshFeedCommand executing]
                                 filter:^BOOL(NSNumber *executing) {
                                     return executing.boolValue;
                                 }] mapReplace:@"Please wait, loading..."];
    
    // list is empty because the request finished but no results were returned
    RACSignal *emptySignal = [[[RACObserve(self, photos) ignore:nil]
                               filter:^BOOL(NSArray *array) {
                                   return array.count == 0;
                               }]
                              mapReplace:@"There are no photos to display. Tap to refresh."];
    
    // list is empty because inital request errored out
    RACSignal *errors = [self.refreshFeedCommand.errors mapReplace:@"There was an error loading the photos... Tap to retry."];
    
    RAC(self, emptyStateText) = [RACSignal merge:@[loadingSignal, emptySignal, errors]]; // merge all of the signals and set the text.
    
}

#pragma mark - AHPhotoFeedViewModelProtocol

- (id<AHPhotoViewModelProtocol>)viewModelForPhotoAtIndexPath:(NSIndexPath *)indexPath {
    AHPhotoViewModel *viewModel = [AHPhotoViewModel viewModelWithPhoto:self.photos[indexPath.section]];
    
    return viewModel;
}

- (NSUInteger)numberOfItemsInSection:(NSUInteger)section {
    return 1;
}

- (NSUInteger)numberOfSections {
    return self.photos.count;
}

@end
