//
//  main.m
//  PopularPhotos
//
//  Created by Anthony Hoang on 8/6/15.
//  Copyright (c) 2015 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TestAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        Class appDelegateClass = [AppDelegate class];
        
        // If the app is launched from an XCTestCase file, launch the test app delegate.
        if (NSClassFromString(@"XCTestCase") != nil) {
            appDelegateClass = [TestAppDelegate class];
        }
        return UIApplicationMain(argc, argv, nil, NSStringFromClass(appDelegateClass));
    }
}
