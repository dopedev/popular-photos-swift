//
//  AHPhotosClient.h
//  
//
//  Created by Anthony Hoang on 8/6/15.
//
//

#import <AFHTTPRequestOperationManager+RACSupport.h>
#import "AHPhotoPage.h"

/**
 *  This class is responsbile for all network requests to the 500px service
 */
@interface AHPhotosClient : AFHTTPRequestOperationManager

/**
 *  GET request to the 'popular' endpoint on the Instagram service.
 *
 *  @param maxSize The max number of results that should be returned.
 *  @param page    The page offset to begin the search. For pagination purposes.
 *
 *  @return A signal whose 'next' returns an AHPhotoPage object.
 */
- (RACSignal *)getPageOfPopularPhotos:(NSUInteger)maxSize pageNumber:(NSUInteger)page;


@end
