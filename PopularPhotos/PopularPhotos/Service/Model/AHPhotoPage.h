//
//  AHPhotoPage.h
//  
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import "MTLModel.h"
#import <MTLJSONAdapter.h>
#import "AHPhoto.h"

/**
 *  This keeps track of the pagniation info for the 500px service
 */
@interface AHPhotoPage : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *currentPageNumber;
@property (nonatomic, copy, readonly) NSArray *photos;

@end
