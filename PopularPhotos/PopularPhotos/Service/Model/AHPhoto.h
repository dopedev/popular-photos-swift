//
//  AHPhoto.h
//  
//
//  Created by Anthony Hoang on 8/6/15.
//
//

#import <MTLModel.h>
#import <MTLJSONAdapter.h>

@interface AHPhoto : MTLModel <MTLJSONSerializing>

/**
 *  The number of comments left on the photo.
 */
@property (nonatomic, strong, readonly) NSNumber *commentCount;

/**
 *  The number of 'likes' the photo received.
 */
@property (nonatomic, strong, readonly) NSNumber *favoritesCount;

/**
 *  The date the photo was created.
 */
@property (nonatomic, strong, readonly) NSDate *dateCreated;

/**
 *  The caption for the photo.
 */
@property (nonatomic, copy, readonly) NSString *caption;

/**
 *  The URL to download the photo.
 */
@property (nonatomic, copy, readonly) NSURL *imageURL;

@end
