//
//  AHZoomTransition.h
//  
//
//  Created by Anthony Hoang on 8/8/15.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AHPhotoCell.h"

/**
 *  This is a "zooming" transition to make it looks like a view controller is "expanding" from the cell that was clicked.
 */
@interface AHZoomTransition : NSObject <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning>

/**
 *  Creates a "reference" imageview to reference the position of the cell that was clicked. The animation will then take this reference origin and "expand" from that origin, making it look like the transision is "expanding" from the clicked cell.
 *
 *  @param cell      The cell that was clicked.
 *  @param indexPath The index path of the cell taht was clicked.
 *  @param tableView The table view the cell came from. This is used for converting the rect.
 *  @param view      The view the tableview is a child of. This is used for converting the rect.
 */
- (void)setExpandingCell:(AHPhotoCell *)cell
               indexPath:(NSIndexPath *)indexPath
           fromTableView:(UITableView *)tableView
                  inView:(UIView *)view;

@end
