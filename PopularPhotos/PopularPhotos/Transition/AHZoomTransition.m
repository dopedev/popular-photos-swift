//
//  AHZoomTransition.m
//
//
//  Created by Anthony Hoang on 8/8/15.
//
//

#import "AHZoomTransition.h"
#import "AHPhotoDetailViewController.h"
#import <Masonry.h>
#import <RACEXTScope.h>

static NSTimeInterval const kDefaultTransitionDuration = 0.2;

@interface AHZoomTransition ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation AHZoomTransition

- (instancetype)init {
    self = [super init];
    
    if (self) {
        
    }
    
    return self;
}

- (void)setExpandingCell:(AHPhotoCell *)cell
               indexPath:(NSIndexPath *)indexPath
           fromTableView:(UITableView *)tableView
                  inView:(UIView *)view {
    
    self.imageView = [UIImageView new];
    self.imageView.image = cell.photoImageView.image;
    
    CGPoint origin = [tableView rectForRowAtIndexPath:indexPath].origin;
    CGSize size = cell.photoImageView.frame.size;
    
    
    CGRect rect = CGRectMake(origin.x, origin.y, size.width, size.height);
    
    self.imageView.backgroundColor = [UIColor redColor];
    CGRect convertedRect = [tableView convertRect:rect toView:tableView.superview];
    self.imageView.frame = convertedRect;
    
}

#pragma mark - UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return kDefaultTransitionDuration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    UIViewController* toViewController = (AHPhotoDetailViewController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    [[transitionContext containerView] addSubview:toViewController.view];
    [transitionContext completeTransition:YES];

    if ([toViewController isKindOfClass:[AHPhotoDetailViewController class]]) {
        // set the photo's frame to the "reference" frame so it appears in the same origin as the cell in the list view
        AHPhotoDetailViewController *detailViewController = (AHPhotoDetailViewController *)toViewController;
        detailViewController.imageView.frame = self.imageView.frame;
        
        // animate the original constraints.
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 usingSpringWithDamping:1 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [detailViewController.view layoutIfNeeded];
            [detailViewController.imageView layoutIfNeeded];
            [detailViewController.view addConstraints:detailViewController.imageView.constraints];
            
            [detailViewController.imageView layoutIfNeeded];
            [detailViewController.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
        }];
    }
}

@end